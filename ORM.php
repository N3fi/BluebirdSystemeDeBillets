<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of modelObject
 *
 * @author nefi
 */
class ORM extends PDO{
    private static $instance;
    const dsn = 'mysql:host=localhost;dbname=maBase';
    const username = 'root';
    const password = 'psw';
    
    private function __construct(){
        parent::__construct(self::dsn, self::username, self::password);
    }
    private function __clone(){}
    public static function getInstance(){
        if(self::$instance==NULL){
            self::$instance=new self();
        }
        return self::$instance;
    }
    public function getList($className,$filter='order by id'){
        $ins = $this->prepare('select * from '.$className.' '.$filter);
        $ins->setFetchMode(PDO::FETCH_CLASS, $className);
        $ins->execute();
        while ($object = $ins->fetch())
        {
          $result[] = clone $object;
        }
        
        return $result;
    }
    public function getItemById($className,$id){
        $ins = $this->prepare('select * from '.$className.' where id=?');
        $ins->setFetchMode(PDO::FETCH_CLASS, $className);
        $ins->execute($id);
        $object = $ins->fetch();
        return clone $object;
    }
    
    public function addItem($className,$properties){
        $propertiesString = '';
        $valuesIndex='';
        $firstPropertie = TRUE;
        $values=array();
        
        foreach ($properties as $property => $value) {
            $propertiesString += $firstPropertie?$property:','.$property;
            $valuesIndex += $firstPropertie?'?':',?';
            array_push($values, $value);
            $firstPropertie=FALSE;
        }
        $ins = $this->prepare('insert into '.$className.' ('.$propertiesString.') values('.$valuesIndex.')');
        $ins->execute($values); 
    }
}
