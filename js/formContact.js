/* 
 * UTILISATION
 * ===========
 * 1. Ajouter une class "choices" sur les balises "select" que l'on souhaite 
 * utiliser pour proposer les choix du formulaire.
 * 2. Ajouter sur les options de la balise "select" l'attribut "data-content".
 * 3. Donner comme valeur à l'attribut "data-content" les éléments que l'on 
 * souhaite afficher sur la page séparé par un point-virgule.
 * Les éléments que l'on souhaite se déclare ainsi : 
 * - Pour avoir tous les éléments appartenant au même tagName : nom_tagname
 * - Pour avoir tous les éléments d'une classe : .nom_de_la classe
 * - Pour avoir l'élément à partir d'un id : #nom_de_l'_id
 * 
 * Exemple:
 * --------
 * <select>
 *      <option value="">Merci de sélectionner votre problème</option>
 *      <option data-content="#div0" value="0">test0</option>
 *      <option data-content="#div1" value="1">test1</option>
 *      <option data-content="#div2" value="2">test2</option>
 * </select>
 * <div id="div0" class="choices">
 *      Test 0
 *      <select>
 *          <option value="">Merci de préciser une catégorie</option>
 *          <option data-content="#div0;#div0A" value="0A">test0A</option>
 *          <option data-content="#div0;#div0B" value="0B">test0B</option>
 *          <option data-content="#div0;#div0C" value="0C">test0C</option>
 *      </select>
 * </div>
 */

window.onload = function(){
    resetOptions([]);
    [].slice.call(document.getElementsByTagName('select')).forEach(function (select){
        select.onchange = function (e){
            var objs=strg2HtmlObjs(e.target.selectedOptions[0].getAttribute('data-content')); 
            resetOptions(objs);
            objs.forEach(function (obj){
                obj.style.display='block';
            });
        };
    });
};
function resetOptions(excepts){
    var elements = [].slice.call(document.getElementsByClassName('choices'));
    filterList(elements,excepts).forEach(function(select){
        select.value="";
    });
    elements.forEach(function(element){
        element.style.display='none';
    });
}
function strg2HtmlObjs(str){
    var strgs = str.split(';');
    var objs = [];
    strgs.forEach(function(strg){
        if(strg.slice(0,1)==='#'){
            objs.push(document.getElementById(strg.slice(1,strg.length)));
        } else if(strg.slice(0,1)==='.') {
            objs = objs.concat([].slice.call(document.getElementsByClassName(strg.slice(1,strg.length))));
        } else {
            try{
              objs = objs.concat([].slice.call(document.getElementsByTagName(strg))); 
            } catch(e){
                console.log('Erreur :'+e.toString());
            }
        }
    });   
    return objs;
}
function filterList(datas,elementsToPull){
    var elementsFiltred=[];var i=0;
    if(datas.length>0){
        datas.forEach(function(elementsToFilter){
            elementsFiltred[i++]=elementsToFilter.getElementsByTagName('select')[0];
        });
    }
    elementsToPull.forEach(function(elementToPull){
        elementsFiltred=elementsFiltred.filter(function(elmt){
            return elmt!==elementToPull.getElementsByTagName('select')[0];
        });
    });
    return elementsFiltred;
}