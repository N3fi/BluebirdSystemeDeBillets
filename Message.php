<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Message{
    private $text;
    private $isSendedByUser;
    private $date;
    private $timeZone;
    private $ticketId;
    function __construct($_text,$_isSendedByUser) {
        parent::__construct();
        $this->setText($_text);
        $this->setIsSendedByUser($_isSendedByUser);
        $this->timeZone=date_default_timezone_set('Europe/Paris');
        $this->date('d/m/Y H:i:s', time());
    }
    private function setText($_text){
        if(is_string($_text)){
            $this->text=$_text;
        }
    }
    private function setIsSendedByUser($_isSendedByUser){
        if(is_bool($_isSendedByUser)){
            $this->isSendedByUser=$_isSendedByUser;
        }
    }
    public function getText(){
        return $this->text;
    }
    public function getIsSendedByUser(){
        return $this->isSendedByUser;
    }
    public function getDate(){
        return $this->date;
    }
    public function setTicketId($_idTicket){
        if(is_int($_idTicket)){
            $this->ticketId=$_idTicket;
        }
    }
    public function getTicketId(){
        return $this->ticketId;
    }
    public function save($_text,$_isSendedByUser,$_idTicket){
        $this->setText($_text);
        $this->setIsSendedByUser($_isSendedByUser);
        $this->setTicketId($_idTicket);
        $orm=new ORM();
        $orm->addItem(get_class($this), ['ticketId'=> $this->ticketId,'text' => $this->text,'isSendedByUser' => $this->isSendedByUser,'date' => $this->date]);
    }
}