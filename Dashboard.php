<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

abstract class Dashboard{
    private $tickets;
    public function __construct() {
        $this->loadTickets();
    }
    protected abstract function loadTickets();
    
    protected function setTickets($_tickets){
        if(is_array($_tickets)){
            $this->tickets=$_tickets;
        }
    }
}